consumer_conf = {
    'bootstrap.servers': 'localhost:9092',
    'group.id': 'test-group',
    'enable.auto.commit': 'false'
}