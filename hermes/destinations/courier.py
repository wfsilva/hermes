from .kinesis_courier import KinesisCourier
from .void_courier import VoidCourier


class Courier:
    """Represents a generic courier, implementing the courier interface. This
    class should be capable of instantiating every available courier

    Attributes
    ----------
    courier : any available courier object

    Methods
    -------
    collect_messages: get a pointer to the message buffer
    deliver: basic interface to persist data

    """

    def __init__(self, courier_type, **kwargs):
        self.courier = self._get_courier(courier_type, **kwargs)

    def _get_courier(self, courier_type, **kwargs):
        if courier_type == 'kinesis':
            return KinesisCourier(
                stream=kwargs.get('stream'),
                region=kwargs.get('region', 'us-east-1')
            )
        elif courier_type == 'void':
            return VoidCourier()

    def collect_messages(self, buffer):
        self.courier.collect_messages(buffer)

    def deliver(self):
        self.courier.deliver()
