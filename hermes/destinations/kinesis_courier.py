import boto3
from datetime import datetime
import json
import logging

DEFAULT_STREAM = 'test-hermes-kafka-stream'


class KinesisCourier:
    """Courier that persists data to AWS Kinesis Data Streams"""

    def __init__(self, stream=None, region='us-east-1'):
        self.stream = stream if stream else DEFAULT_STREAM
        self.region = region
        self.client = self._get_client(region)
        self.messages = []

    def _get_client(self, region):
        kinesis = boto3.client('kinesis', region_name=region)
        return kinesis

    def collect_messages(self, buffer):
        self.messages = buffer

    def deliver(self):

        response = self.client.put_records(
            Records=[
                {
                    'Data': json.dumps(msg)+'\n',
                    'PartitionKey': str(datetime.now().timestamp())
                } for msg in self.messages
            ],
            StreamName=self.stream
        )
        logging.debug(f'KinesisCourier delivery response: {response}')
        self.messages = []

        return response
