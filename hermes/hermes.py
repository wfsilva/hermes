from config.schema_registry import sr_conf
from config.consumer import consumer_conf
from confluent_kafka import TopicPartition
from origins.kafka.avro_consumer import AvroConsumer
from origins.kafka.offset.offset_persister import OffsetPersister
from destinations.courier import Courier
import logging


logging.basicConfig(level=logging.INFO)


def _on_revoke(consumer, partitions):
    pass


def _on_assign(consumer, partitions):
    topic = partitions[0].topic
    partition_numbers = [p.partition for p in partitions]
    op = OffsetPersister('dynamodb', 'test-hermes-topic')
    loff = op.get_last_offsets(partition_numbers)
    consumer.assign(
        [TopicPartition(topic, part, offs) for part, offs in loff.items()]
    )
    print(f'New partition assignment: {loff}')


def main():

    offps = OffsetPersister('dynamodb', 'test-hermes-topic')
    cour = Courier('kinesis')

    ac = AvroConsumer(
        sr_conf=sr_conf,
        consumer_conf=consumer_conf,
        topic='test-hermes-topic',
        courier=cour,
        offset_persister=offps,
        value_schema_subject='test-hermes-topic-value',
        key_schema_subject='test-hermes-topic-key'
    )

    ac.subscribe(on_assign=_on_assign, on_revoke=_on_revoke)
    ac.consume()


if __name__ == '__main__':
    main()
