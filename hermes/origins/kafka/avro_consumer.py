from collections import namedtuple
from confluent_kafka.avro.serializer import SerializerError
from confluent_kafka import TopicPartition
from confluent_kafka import DeserializingConsumer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroDeserializer

MAX_INGEST_SIZE = 500  # Kinesis

Offset = namedtuple('Offset', 'offset persisted')


class AvroConsumer:
    """Builds an AvroConsumer on the top of a DeserializingConsumer

    Attributes
    ----------
    schema_registry_client : self-explanatory
    consumer: a DeserializingConsumer object instance
    topic: an Apache Kafka topic name
    courier: a generic Courier instance
    offset_persister: a generic OffsetPersister instance
    value_schema_subject: Schema Registry value subject name
    key_schema_subject: Schema Registry key subject name

    """

    def __init__(
        self,
        sr_conf,
        consumer_conf,
        topic,
        courier,
        offset_persister,
        value_schema_subject,
        key_schema_subject=None
    ):
        self.schema_registry_client = SchemaRegistryClient(sr_conf)
        self.consumer = self._get_consumer(
            consumer_conf,
            value_schema_subject,
            key_schema_subject
        )
        self.topic = topic
        self.courier = courier
        self.offset_persister = offset_persister
        self.max_offsets = {}

    def _get_consumer(
        self,
        consumer_conf,
        value_schema_subject,
        key_schema_subject
    ):
        if key_schema_subject:
            key_schema_str = self.schema_registry_client \
                .get_latest_version(key_schema_subject) \
                .schema.schema_str
            avro_key_deserializer = AvroDeserializer(
                key_schema_str,
                self.schema_registry_client
            )
            consumer_conf['key.deserializer'] = avro_key_deserializer

        value_schema_str = self.schema_registry_client \
            .get_latest_version(value_schema_subject) \
            .schema.schema_str
        avro_value_deserializer = AvroDeserializer(
            value_schema_str,
            self.schema_registry_client
        )

        consumer_conf['value.deserializer'] = avro_value_deserializer

        return DeserializingConsumer(consumer_conf)

    def subscribe(self, on_assign=None, on_revoke=None):
        """Subscribe the consumer instance to an Apache Kafka topic"""
        self.consumer.subscribe(
            [self.topic],
            on_assign=on_assign,
            on_revoke=on_revoke
        )

    def commit_offsets(self):
        """Commit last consumed message offsets to Apache Kafka topic"""
        offsets_to_commit = []
        for partition, offset in self.max_offsets.items():
            if not offset.persisted:
                offsets_to_commit.append(
                    TopicPartition(self.topic, partition, offset.offset)
                )
                self.max_offsets[partition] = Offset(
                    offset=offset.offset, persisted=True
                )
        self.consumer.commit(offsets=offsets_to_commit)

    def consume(self):
        """Executes the consumer infinite loop"""

        buffer = []

        while True:

            try:
                msg = self.consumer.poll(5)
            except SerializerError as e:
                print("Deserialization failed for {}: {}".format(msg, e))
                break

            if msg is None:
                if buffer:
                    self.courier.collect_messages(buffer)
                    self.courier.deliver()
                    self.offset_persister.persist(self.max_offsets)
                    self.commit_offsets()
                    buffer = []
                continue

            if msg.error():
                print("AvroConsumer error: {}".format(msg.error()))
                continue

            next_offset = msg.offset() + 1
            self.max_offsets[msg.partition()] = Offset(
                offset=next_offset, persisted=False
            )

            buffer.append(msg.value())

            if len(buffer) == MAX_INGEST_SIZE:
                self.courier.collect_messages(buffer)
                self.courier.deliver()
                self.offset_persister.persist(self.max_offsets)
                self.commit_offsets()
                buffer = []
