import boto3
from boto3.dynamodb.conditions import Key, Attr
import logging

DEFAULT_KAFKA_OFFSETS_TABLE = 'test-kafka-offsets'


class DynamoDBOffsetPersister:
    """Persists Apache Kafka offsets to an AWS DynamoDB table"""

    def __init__(self, topic, tbl_name=None, region='us-east-1'):
        self.topic = topic
        table_name = tbl_name if tbl_name else DEFAULT_KAFKA_OFFSETS_TABLE
        self.table = self._get_table(table_name, region)

    def _get_table(self, table_name, region):
        dynamodb = boto3.resource('dynamodb', region_name=region)
        return dynamodb.Table(table_name)

    def persist(self, offsets):
        for partition, offset in offsets.items():
            if not offset.persisted:
                response = self.table.update_item(
                    Key={
                        'topic': self.topic,
                        'partition': partition,
                    },
                    UpdateExpression="set partition_offset = :off",
                    ExpressionAttributeValues={':off': offset.offset},
                    ReturnValues="NONE"
                )
                logging.debug(f'DynamoDB persist response: {response}')

        persisted_offsets = [
            (k, v.offset) for k, v in offsets.items()
            if v.persisted is False
        ]

        logging.info(f'Persisted offsets: {persisted_offsets}')

    def get_last_offsets(self, partition_numbers):

        response = self.table.scan(
            FilterExpression=Key('topic').eq(self.topic) &
            Attr('partition').is_in(partition_numbers)
        )
        logging.debug(f'DynamoDB get offsets response: {response}')

        last_offsets = {
            int(i['partition']): int(i['partition_offset'])
            for i in response['Items']
        }
        return last_offsets
