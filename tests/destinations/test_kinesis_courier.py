from hermes.destinations.kinesis_courier import KinesisCourier
import pytest
from unittest.mock import patch
import boto3
from time import sleep

dev_kinesis = boto3.client(
    'kinesis',
    region_name='us-east-1',
    endpoint_url='http://localhost:4566',
    aws_access_key_id='test',
    aws_secret_access_key='test'
)


def get_dev_client(self, region):
    return dev_kinesis


@pytest.fixture(scope='module')
def create_stream(request):
    dev_kinesis.create_stream(
        StreamName='test-hermes-kafka-stream',
        ShardCount=4
    )
    sleep(2)  # Wait for stream creation

    def delete_stream():
        dev_kinesis.delete_stream(
            StreamName='test-hermes-kafka-stream',
            EnforceConsumerDeletion=True
        )

    request.addfinalizer(delete_stream)


def test_collect_messages():
    buffer = [{'foo': 'bar'}, {'foo': 'baz'}]
    kc = KinesisCourier()
    kc.collect_messages(buffer)
    assert kc.messages is buffer


@patch.object(
    KinesisCourier,
    '_get_client',
    get_dev_client
)
def test_deliver(create_stream):
    buffer = [{'foo': 'bar'}, {'foo': 'baz'}]
    kc = KinesisCourier()
    kc.collect_messages(buffer)
    r = kc.deliver()
    assert r['ResponseMetadata']['HTTPStatusCode'] == 200
    assert not kc.messages
